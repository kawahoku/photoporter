package main;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import file_tree.FileTree;
import view.Library;
import view.Viewer;

/*
 * 	panel0
 * 		menubar(PAGE_START)
 * 		split(CENTER)
 * 			filetree
 *  		panel1_2
 *  			split1_2
 *  				viewer
 *  				panel1_2_2
 *  					scroll1_2_2
 * 							library
 */
public class Contents extends JFrame
{
	private JMenuBar menubar;
	
	private JPanel panel0;
	private JSplitPane split0;

	private FileTree filetree;
	private JPanel panel1_2;
	private JSplitPane split1_2;
	
	private Viewer viewer;

	private JPanel panel1_2_2;
	private JScrollPane scroll1_2_2;
	private Library library;
	

	public Contents()
	{
		panel0 = new JPanel(new BorderLayout());
		menubar = new JMenuBar();
		panel0.add(menubar, BorderLayout.PAGE_START);

		getContentPane().add(panel0);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		SwingUtilities.invokeLater(() -> {
			initialize();
			keyBinding();
		}); 
	}
	
	public void keyBinding()
	{
		panel0.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("control S"), "SAVE");
		panel0.getActionMap().put("SAVE", new SaveAction());
	}
	
	public class SaveAction extends AbstractAction
	{
		public SaveAction()
		{
		}
		public void actionPerformed(ActionEvent e)
		{
			new SaveDialog(filetree.getRoot());
		}
	}
	
	public void initialize()
	{
		filetree = new FileTree(this);
		panel1_2 = new JPanel(new BorderLayout());
		split0 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, filetree, panel1_2);
		split0.setResizeWeight(0.15);
		panel0.add(split0, BorderLayout.CENTER);
		
		viewer = new Viewer(this);
		panel1_2_2 = new JPanel(new BorderLayout());
		split1_2 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, viewer, panel1_2_2);
		split1_2.setResizeWeight(0.7);
		panel1_2.add(split1_2, BorderLayout.CENTER);
		
		library = new Library(this);
		scroll1_2_2 = new JScrollPane(
				library,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		panel1_2_2.add(scroll1_2_2, BorderLayout.CENTER);
	}
	
	public Viewer getViewer()
	{
		return viewer;
	}
	
	public Library getLibrary()
	{
		return library;
	}
}
