package view;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ext.StretchIcon;
import main.Contents;
import utils.Imager;

public class Viewer extends JPanel
{
	private Contents contents;
	private JLabel view;
	private JLabel label;
	private Photo photo;
	
	private ExecutorService exec = Executors.newSingleThreadExecutor(r -> {
		Thread t = new Thread(r);
		t.setDaemon(true);
		return t;
	});
	
	public Viewer(Contents c)
	{
		super(new BorderLayout());
		contents = c;

		view = new JLabel();
		label = new JLabel();
		label.setHorizontalAlignment(SwingConstants.CENTER);
		add(view, BorderLayout.CENTER);
		add(label, BorderLayout.PAGE_END);
		addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e)
			{
				if( photo != null ) photo.setSelected(!photo.isSelected());
			}
		});
		addMouseWheelListener(new MouseWheelListener() {
			public void	mouseWheelMoved(MouseWheelEvent e)
			{
				Library library = contents.getLibrary();
				if( library != null )
				{
					if( e.getWheelRotation() < 0 ) library.turnPhoto(-1);
					if( e.getWheelRotation() > 0 ) library.turnPhoto(+1);
				}
			}
		});
	}
	
	public void setPhoto(Photo photo)
	{
		this.photo = photo;
		ImageIcon icon = new StretchIcon(photo.getImage());
		view.setIcon(icon);
		label.setText( ( new File(photo.getSource()) ).getName() );
		load(photo);
	}
	
	private void load(Photo photo)
	{
		exec.execute(() -> {
			if( photo == contents.getLibrary().getActive() )
			{
				Image image = Imager.getImage(photo.getSource());
				ImageIcon icon = new StretchIcon(image);
				if( photo == contents.getLibrary().getActive() )
				{
					view.setIcon(icon);
				}
			}
		});
	}
}
