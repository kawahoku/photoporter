package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import ext.StretchIcon;
import utils.Imager;

public class Photo extends JPanel
{
	public static final int width = 200;
	public static final int height = 200;
	public static final Color activecolor = new Color(255, 128, 128);
	public static final Color inactivecolor = new Color(240, 240, 240);
	public static final int viewpad = 2;
	public static final Border activeBorder = BorderFactory.createLineBorder(Color.red);
	public static final Border inactiveBorder = BorderFactory.createLineBorder(Color.white);

	private JLabel view;
	private JLabel label;
	
	private boolean selected = false;
	private boolean active = false;
	private boolean loaded = false;
	private boolean submitted = false;
	
	private String src;
	
	
	public Photo(File file)
	{
		super(new BorderLayout());
		src = file.toString();
		
		view = new JLabel();
		view.setBorder(BorderFactory.createEmptyBorder(viewpad, viewpad, viewpad, viewpad));
		view.setOpaque(true);
		label = new JLabel();
		label.setHorizontalAlignment(SwingConstants.CENTER);
		add(view, BorderLayout.CENTER);
		add(label, BorderLayout.PAGE_END);
		
		addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e)
			{
				switch( e.getClickCount() )
				{
				case 1: setSelected(!selected); break;
				case 2:	if( getParent() != null )
							((Library)getParent()).turnPhoto(Photo.this);
						break;
				}
			}
		});
	}
	
	public void load()
	{
		System.out.println("load :" + getSource());
		File file = new File(src);
		Image image = Imager.getScaledImage(file, width, height);
		view.setIcon(new StretchIcon(image));
		label.setText(file.getName());
		loaded = true;
	}
	
	public boolean isSelected()
	{
		return selected;
	}
	
	public void setSelected(boolean flag)
	{
		selected = flag;
		System.out.println(flag);
		if( flag == true )
		{
			view.setBackground(activecolor);
		}
		else
		{
			view.setBackground(inactivecolor);
		}
	}
	
	public void setActive(boolean flag)
	{
		active = flag;
		if( active )
		{
			setBorder(activeBorder);
		}
		else
		{
			setBorder(inactiveBorder);
		}
	}
	
	public boolean isLoaded()
	{
		return loaded;
	}
	
	public boolean isSubmitted()
	{
		return submitted;
	}
	
	public void setSubmitted(boolean flag)
	{
		submitted = flag;
	}

	public Image getImage()
	{
		return ((ImageIcon)view.getIcon()).getImage();
	}
	
	public String getSource()
	{
		return src;
	}
}
